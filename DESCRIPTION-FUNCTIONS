intbinomial - binomial confidence interval
intexponential - exponential confidence interval
intnormalm - normal confidence interval for μ
intnormals - normal confidence interval for σ
intpoisson - Poisson confidence interval
cdfbeta - beta type 1 cdf
cdfbeta2 - beta type 2 cdf
cdfbinomial - binomial cdf
cdfchi2 - χ2 (central and non-central) cdf
cdfcv - sample coefficient of variation cdf
cdfdphase - discrete Phase-Type cdf
cdfexponential - exponential cdf
cdffisher - Fisher (central and non-central) cdf
cdffoldednormal - folded normal cdf
cdfgamma - gamma cdf
cdfgev - generalized Extreme Value cdf
cdfhypergeometric - hypergeometric cdf
cdfjohnson - Johnson’s cdf
cdflognormal - lognormal cdf
cdfmedian - normal sample median cdf
cdfnormal - normal cdf
cdfpareto - Pareto cdf
cdfpascal - Pascal cdf
cdfpoisson - Poisson cdf
cdfrnge - normal range cdf
cdfstandev - normal sample standard-deviation cdf
cdfstudent - Student (central and non-central) cdf
cdfweibull - Weibull cdf
boxbehnken - Box-Behnken designs
boxcoxlinear - Box-Cox linearity transformation
centralcomposite - central composite designs
coded2natural - coded to natural variables
doxpand - design expansion
doxptim - design optimisation
equiradial - equiradial designs
factorial2 - two levels full and fractional factorial designs
mulreg - multilinear regression analysis
mulregdisp - multilinear regression analysis results display
mulregplot - multilinear regression analysis results plot
natural2coded - natural to coded variables
plackettburman - Plackett-Burman designs
simpdex - simplex designs
fitbeta - beta type 1 parameters estimation
fitgamma - gamma parameters estimation
fitgev - generalized Extreme Value parameters estimation
fitjohnson - Johnson parameters estimation
fitlognormal - lognormal parameters estimation
fitweibull - Weibull parameters estimation
idfbeta - beta type 1 idf
idfbeta2 - beta type 2 idf
idfchi2 - χ2 (central and non-central) idf
idfcv - sample coefficient of variation idf
idfexponential - exponential idf
idffisher - Fisher (central and non-central) idf
idfgamma - gamma idf
idfgev - generalized Extreme Value idf
idfjohnson - Johnson’s idf
idflognormal - lognormal idf
idfmedian - normal sample median idf
idfnormal - normal idf
idfpareto - Pareto idf
idfstandev - normal sample standard-deviation idf
idfstudent - Student (central and non-central) idf
idfweibull - Weibull idf
allcombination - matrix element combinations
allpermutation - matrix element permutations
arrangement - number Ap of arrangements
combination - number Cn of combinations
confhyper - confluent hypergeometric function
depth - non parametric multivariate depth
hausdorff - Hausdorff (median) distance between polylines
lowess - LOcally WEighted Scatterplot Smoothing
momdphase - first moments of a Discrete Phase-Type distribution
nearestneighbors - find the k nearest neighbors
neldermead - Nelder Mead’s downhill simplex nonlinear optimization algorithm
savitzkygolay - Savitzky-Golay smoothing filter
simplex - simplex computation
simplexolve - solve a system of non-linear equations
torczon - Torczon’s multidirectional nonlinear optimization algorithm
vandercorput - Van der Corput’s sequence
boxplot - Box plot
qplot - quantile plot
qqplot - quantile-quantile plot
pdfbeta - beta type 1 pdf
pdfbeta2 - beta type 2 pdf
pdfbinomial - binomial pdf
pdfchi2 - χ2 (central and non-central) pdf
pdfcp - CP pdf
pdfcpk - CP K pdf
pdfcpm - CP M pdf
pdfcpmk - CP M K pdf
pdfcpuv - V ̈nnman’s Cp (u, v) pdf
pdfcv - sample coefficient of variation pdf
pdfdphase - discrete phase-type pdf
pdfexponential - exponential pdf
pdffisher - Fisher (central and non-central) pdf
pdffoldednormal - folded normal pdf
pdfgamma - gamma pdf
pdfgev - generalized Extreme Value pdf
pdfhypergeometric - hypergeometric pdf
pdfkernel - kernel smoothed pdf
pdfjohnson - Johnson’s pdf
pdflognormal - lognormal pdf
pdfmedian - normal sample median pdf
pdfmultinormal - multinormal pdf
pdfnormal - normal pdf
pdfpareto - Pareto pdf
pdfpascal - Pascal pdf
pdfpoisson - Poisson pdf
pdfrnge - normal range pdf
pdfstandev - normal sample standard-deviation pdf
pdfstudent - Student pdf
pdfweibull - Weibull pdf
quadhermite - Gauss-Hermite quadrature
quadlaguerre - Gauss-Laguerre quadrature
quadlegendre - Gauss-Legendre quadrature
quadsimpson - Simpson quadrature
rndbeta - beta type 1 random number generator
rndbeta2 - beta type 2 random number generator
rndbinomial - binomial random number generator
rndexponential - exponential random number generator
rndfoldednormal - folded normal random number generator
rndgamma - gamma random number generator
rndgev - generalized Extreme Value random number generator
rndjohnson - Johnson’s random number generator
rndlognormal - lognormal random number generator
rndmultinormal - multinormal random number generator
rndnormal - normal random number generator
rndpareto - Pareto random number generator
rndpascal - Pascal random number generator
rndpoisson - Poisson random number generator
rndstandev - normal sample standard-deviation random number
rndweibull - Weibull random number
generator
autocorrelation - autocorrelation coefficient
bootstrap - bootstrap sampling
correlation - correlation matrix
crosscorrelation - crosscorrelation coefficient
kurtosis - kurtosis coefficient
quantile - quantile
rnge - range
skewness - skewness coefficient
standev - standard deviation
totalmedian - total median coefficients
varcovar - variance-covariance matrix
arlmean - ARL of the mean control chart
arlmeanRR - ARL of the Run Rules mean control chart
arlmedian - ARL of the median control chart
arlmedianRR - ARL of the Run Rules median control chart
arlrnge - ARL of the range control chart
arlstandev - ARL of the standard-deviation control chart
arlstandevRR - ARL of the Run Rules standard-deviation control chart
cp - capability index CP estimation and confidence interval
cpk - capability index CP K estimation and confidence interval
krnge - range coefficients KR (n)
kstandev - standard-deviation coefficients KS (n, r)
mcpshahriari - Shahriari’s multivariate capability index CP
mcptaam - Taam’s multivariate capability index CP
andersondarling - Anderson-Darling’s normality test
ansaribradley - Ansari-Bradley’s test
bartlett - Bartlett’s test
grubbs - Grubbs test
kendall - Kendall’s test
levene - Levene’s test
mardia - Mardia’s test
spearman - Spearman’s test
tstbinomial1 - binomial one sample p test
tstbinomial2 - binomial two samples p test
tstexponential - exponential λ test
tstnormalm1 - normal one sample μ test
tstnormalm2 - normal two samples μ test
tstnormals1 - normal one sample σ test
tstnormals2 - normal two samples σ test
tstsku - normal skewness and kurtosis test
waldwolfowitz - Wald-Wolfowitz’s run test
wilcoxon1 - Wilcoxon’s one sample (paired) test
wilcoxon2 - Wilcoxon’s two samples test
