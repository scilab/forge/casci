//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function K=kstandev(N,r)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  r=1
end
if or((N<2)|(N<2-r)|(N~=floor(N)))
  error("argument ''N'' must be a matrix of integers >= max(2,2-r)")
end
K=exp(gammaln((N-1+r)/2)-gammaln((N-1)/2)+(r/2)*log((2)./(N-1)))
endfunction