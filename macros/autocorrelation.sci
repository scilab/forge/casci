//------------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//------------------------------------------------------------------------------
function c=autocorrelation(X,lag)
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
n=size(X,"*")
if argin==1
  lag=0
end
if or((floor(lag)~=lag)|(abs(lag)>=n))
  error("argument ''lag'' contains invalid lags")
end
X=X-mean(X)
c=zeros(lag)
for j=1:length(lag)
  k=lag(j)
  if k>=0
    c(j)=sum(X(1:n-k).*X(k+1:n))
  else
    c(j)=sum(X(1-k:n).*X(1:n+k))
  end
end
c=c/sum(X.^2)
endfunction