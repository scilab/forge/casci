//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=ansaribradley(X,Y,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if argin==2
  t="~"
end
m=size(X,"*")
n=size(Y,"*")
if modulo(m+n,2)==0
  e=m*(m+n+2)/4
  s=sqrt(m*n*(m+n+2)*(m+n-2)/(48*(m+n-1)))
else
  e=m*(m+n+1)^2/(4*(m+n))
  s=sqrt(m*n*(m+n+1)*(3+(m+n)^2)/(48*(m+n)^2))
end
X=X-median(X)
Y=Y-median(Y)
Z=[X(:);Y(:)]
[u,i1]=sort(Z)
[u,j1]=sort(-i1)
[u,i2]=sort(-Z)
[u,j2]=sort(-i2)
j=min(j1,j2)
if t==">"
  pv=cdfnormal(sum(j(1:m)),e,s)
elseif t=="<"
  pv=1-cdfnormal(sum(j(1:m)),e,s)
elseif t=="~"
  pv=cdfnormal(sum(j(1:m)),e,s)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction