//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfjohnson(Y,s,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=6
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all entries of argument ''Y'' must be in (0,1)")
end
if b<=0   
  error("argument ''b'' must be > 0")
end
if d<=0
  error("argument ''d'' must be > 0")
end
if s=="U"
  X=c+d*sinh((idfnormal(Y)-a)/b)
elseif s=="B"
  X=c+d./(exp((a-idfnormal(Y))/b)+1)
else
  error("argument ''s'' must be ""B"" or ""U""")
end
endfunction