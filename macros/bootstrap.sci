//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=bootstrap(sz,X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    row=sz(1)
    col=1
  case 2
    row=sz(1)
    col=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (row<=0)|(row~=floor(row))|(col<=0)|(col~=floor(col))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
Y=matrix(X(grand(row,col,"uin",1,size(X,"*"))),row,col)
endfunction