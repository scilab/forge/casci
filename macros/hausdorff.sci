//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function h=hausdorff(X,Y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
[nx,p]=size(X)
[ny,py]=size(Y)
if p~=py
  error("arguments ''X'' and ''Y'' have incompatible sizes")
end
q=0.5
n=nx+ny
d2=matrix(sum((X.*.ones(ny,1)-ones(nx,1).*.Y).^2,"c"),ny,nx)
dh=-sort(-[min(d2,"r"),min(d2,"c")'])
h=sqrt(median(dh))
endfunction