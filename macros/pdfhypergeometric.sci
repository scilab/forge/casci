//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfhypergeometric(X,n,p,N)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=4
  error("incorrect number of arguments")
end
if (N<=0)|(N~=floor(N))
  error("argument ''N'' must be an integer >= 1")
end
if (n<=0)|(n>N)|(n~=floor(n))
  error("argument ''n'' must be an integer in {1,..,N}")
end
if (p<0)|(p>1)
  error("argument ''p'' must be in [0,1]")
end
Np=N*p
if Np~=floor(Np)
  error("''N*p'' must be an integer >= 1")
end
Nq=N*(1-p)
mi=max(0,n-Nq)
ma=min(n,Np)
Y=zeros(X)
if p==0
  i=find(X==mi)
  Y(i)=1
elseif p==1
  i=find(X==ma)
  Y(i)=1
else
  i=((X>=mi)&(X<=ma)&(X==floor(X)))
  if or(i)
    Xi=X(i)
    Y(i)=exp(gammaln(Np+1)+gammaln(Nq+1)+gammaln(n+1)+gammaln(N-n+1)-...
             gammaln(Xi+1)-gammaln(Np-Xi+1)-gammaln(n-Xi+1)-...
             gammaln(Nq-n+Xi+1)-gammaln(N+1))
  end
end
endfunction