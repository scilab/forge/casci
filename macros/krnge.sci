//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function K=krnge(N)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if or((N<=1)|(N~=floor(N)))
  error("argument ''N'' must be a matrix of integers >= 2")
end
[u,w]=quadlaguerre(127)
nn=size(N,"*")
K=N
for i=1:nn
  K(i)=sum(u.*pdfrnge(u,N(i)).*w)
end
endfunction