//------------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//------------------------------------------------------------------------------
function c=crosscorrelation(X,Y,lag)
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
X=X(:)
Y=Y(:)
n=size(X,"*")
if (size(Y,"*")~=n)
  error("argument ''X'' and argument ''Y'' have incompatible sizes")
end
if argin==2
  lag=0
end
if or((floor(lag)~=lag)|(abs(lag)>=n))
  error("argument ''lag'' contains invalid lags")
end
X=X-mean(X)
Y=Y-mean(Y)
c=zeros(lag)
for j=1:length(lag)
  k=lag(j)
  if k>=0
    c(j)=sum(X(1:n-k).*Y(k+1:n))
  else
    c(j)=sum(X(1-k:n).*Y(1:n+k))
  end
end
c=c/sqrt(sum(X.^2)*sum(Y.^2))
endfunction