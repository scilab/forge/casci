//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfgev(X,a,b,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if b<=0
  error("argument ''b'' must be > 0")
end
if a==0
  Y=exp(-exp((c-X)/b))
else
  if a>0
    Y=zeros(X)  
  else
    Y=ones(X)
  end
  Z=1+a*(X-c)/b
  i=(Z>0)
  if or(i)
    Y(i)=exp(-Z(i).^(-1/a))
  end
end
endfunction