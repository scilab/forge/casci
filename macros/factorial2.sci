//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Z=factorial2(k,gen,n0)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>3)
  error("incorrect number of arguments")
end
if (k<1)|(k~=floor(k))
  error("argument ''k'' must be an integer >= 1")
end
if ~exists("gen","local")
  gen=[]
end
f=size(gen,"*")
if k<=f
  error("argument ''gen'' contains too many generators")
end
h=k-f
Z=-ones(1,h)
for i=1:h
  Y=Z
  Y(:,i)=-Y(:,i)
  Z=[Z;Y]
end
for i=1:f
  g=str2code(gen(i))
  if g(1)==45
    si=1
  elseif g(1)==46
    si=-1
  else
    error("argument ''gen'' contains invalid generator")
  end
  u=g(2:length(g))
  if u==[]
    error("argument ''gen'' contains invalid generator")
  end
  u=abs(u)-9
  if or((u<1)|(u>h))
    error("argument ''gen'' contains invalid generators")  
  end
  Z(:,h+i)=si*prod(Z(:,u),"c")
end
if ~exists("n0","local")
  n0=0
end
if (n0<0)|(n0~=floor(n0))
  error("argument ''n0'' must be an integer >= 0")
end
Z=[Z;zeros(n0,k)]
endfunction