//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function arl=arlrnge(tau,n,KL,KU)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=4
  error("incorrect number of arguments")
end
if or(tau<=0)
  error("all elements of argument ''tau'' must be > 0")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if KL<0
  error("argument ''KL'' must be >= 0")
end
if KU<KL
  error("argument ''KU'' must be >= argument ''KL''")
end
pL=cdfrnge(KL./tau,n)
pU=1-cdfrnge(KU./tau,n)
arl=(1)./(pU+pL)
endfunction