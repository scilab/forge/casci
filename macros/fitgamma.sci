//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitgammao(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
Y=(X-c)/b
if (a<=0)|(b<=0)|or(Y<=0)
  f=-%inf
else
  f=sum((a-1)*log(Y)-Y-log(b)-gammaln(a))
end
endfunction
//-----------------------------------------------------------------------------
function [a,b,c]=fitgamma(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if (argout<3)|(argout>4)
  error("incorrect number of output arguments")
end
mu=mean(X)
sd=standev(X)
sk=skewness(X)
a=4/sk^2
b=sd*sk/2
c=min(min(X)-0.1,mu-2*sd/sk)
par=neldermead([a,b,c],fitgammao,list(X),tol=1e-8)
a=par(1)
b=par(2)
c=par(3)
endfunction