//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Z=simpdex(k,n0)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if (k<1)|(k~=floor(k))
  error("argument ''k'' must be an integer >= 1")
end
Z=simplex(zeros(1,k))
if argin==1
  n0=0
end
if (n0<0)|(n0~=floor(n0))
  error("argument ''n0'' must be an integer >= 0")
end
Z=[Z;zeros(n0,k)]
endfunction