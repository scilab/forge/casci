//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [inter,sigma]=intnormals(X,side,level)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>3)
  error("incorrect number of arguments")
end
if ~exists("side","local")
  side="both"
end
if ~exists("level","local")
  level=0.95
end
if (level<=0.5)|(level>=1)
  error("argument ''level'' must be in (0.5,1)")
end
alpha=1-level
n=size(X,"*")
sigma=standev(X)
select side
  case "lower"
    inter=sigma*sqrt((n-1)/idfchi2(1-alpha,n-1))
  case "upper"
    inter=sigma*sqrt((n-1)/idfchi2(alpha,n-1))
  case "both"
    inter(1)=sigma*sqrt((n-1)/idfchi2(1-alpha/2,n-1))
    inter(2)=sigma*sqrt((n-1)/idfchi2(alpha/2,n-1))
  else
    error("argument ''side'' must be ''lower'', ''upper'' or ''both''")
end
endfunction