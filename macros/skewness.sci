//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function sk=skewness(X,cr)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  sk=mean(((X-mean(X))/standev(X)).^3)
elseif cr=="c"
  co=size(X,"c")
  sk=mean(((X-(mean(X,"c")*ones(1,co)))./(standev(X,"c")*ones(1,co))).^3,"c")
elseif cr=="r"
  ro=size(X,"r")
  sk=mean(((X-(ones(ro,1)*mean(X,"r")))./(ones(ro,1)*standev(X,"r"))).^3,"r")
else
  error("argument ''cr'' must be ""c"" or ""r""")
end
endfunction