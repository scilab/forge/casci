//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [x,w]=quadsimpson(n,a,b)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if argout~=2
  error("incorrect number of output arguments")
end
if (modulo(n,2)~=1)|(n<3)
  error("argument ''n'' must be an odd integer >= 3")
end
if a>=b
  error("argument ''a'' must be < argument ''b''")
end
x=linspace(a,b,n)'
w=ones(n,1)
w(2:2:n-1)=4
w(3:2:n-2)=2
w=(x(2)-x(1))*w/3
endfunction