//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idffisher(Y,m,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if (m<=0)|(m~=floor(m))
  error("argument ''m'' must be an integer >= 1")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==3
  nc=0
end
if nc<0
  error("argument ''nc'' must be >= 0")
end
if nc==0
  X=cdff("F",m*ones(Y),n*ones(Y),Y,1-Y)
else
  X=cdffnc("F",m*ones(Y),n*ones(Y),nc*ones(Y),Y,1-Y)
end
endfunction