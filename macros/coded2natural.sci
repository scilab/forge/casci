//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=coded2natural(Z,LU)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
[n,k]=size(Z)
[kk,jj]=size(LU)
if k~=kk
  error("arguments ''Z'' and ''LU'' have incompatible sizes")
end
if jj~=2
  error("argument ''LU'' must have 2 columns")
end
if or(LU(:,1)>=LU(:,2))
  error("values in the 1st column of argument ''LU'' must be < values in the 2nd column")
end
Y=zeros(Z)
for i=1:k
  Y(:,i)=Z(:,i)*(LU(i,2)-LU(i,1))/2+(LU(i,2)+LU(i,1))/2
end
endfunction