//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfpoisson(X,lam)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if lam<=0
  error("argument ''lam'' must be > 0")
end
Y=zeros(X)
i=((X>=0)&(X==floor(X)))
if or(i)
  Y(i)=exp(X(i)*log(lam)-lam-gammaln(X(i)+1))
end
endfunction