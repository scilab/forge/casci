//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfcv(X,n,cv)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if cv<=0
  error("argument ''cv'' must be > 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Y(i)=1-cdfstudent(sqrt(n)./X(i),n-1,sqrt(n)/cv)
end
endfunction
