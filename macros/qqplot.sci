//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function qqplot(X,Y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
n=min(size(X,"*"),size(Y,"*"))
for i=1:n
  qx(i)=quantile(X,i/(n+1))
  qy(i)=quantile(Y,i/(n+1))
end
xymin=min(min(X),min(Y))
xymax=max(max(X),max(Y))
plot2d(qx,qy,-5,rect=[xymin,xymin,xymax,xymax])
plot2d([xymin;xymax],[xymin;xymax],2,frameflag=0)
endfunction