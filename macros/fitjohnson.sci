//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitjohnsonb(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
d=par(4)
Y=X-c
Z=c+d-X
if (b<=0)|(d<=0)|or(Y<=0)|or(Z<=0)
  f=-%inf
else
  f=sum(log(b*d./(Y.*Z))-(a+b*log(Y./Z)).^2/2)
end
endfunction
//-----------------------------------------------------------------------------
function f=fitjohnsonu(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
d=par(4)
Y=(X-c)/d
if (b<=0)|(d<=0)
  f=-%inf
else
  f=sum(log(b/d)-(log(1+Y.^2)+(a+b*asinh(Y)).^2)/2)
end
endfunction
//-----------------------------------------------------------------------------
function [s,a,b,c,d]=fitjohnson(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
alpha=0.01
v=idfnormal(alpha)/3
bet=cdfnormal(v)
xa=quantile(X,alpha)
x1a=quantile(X,1-alpha)
xb=quantile(X,bet)
x1b=quantile(X,1-bet)
r=x1a-x1b
q=xb-xa
t=x1b-xb
u=r*q/t^2
if u<1
  s="B"
  b=-v/acosh(sqrt((1+t/r)*(1+t/q))/2)
  a=b*asinh((t/q-t/r)*sqrt((1+t/r)*(1+t/q)-4)/(2*(1/u-1)))
  d=t*sqrt(((1+t/r)*(1+t/q)-2)^2-4)/(1/u-1)
  c=(xb+x1b-d+t*(t/q-t/r)/(1/u-1))/2
  c=min(c,min(X)-0.1)
  d=max(d,rnge(X)+0.2)
  par=neldermead([a,b,c,d],fitjohnsonb,list(X),tol=1e-6)
else
  s="U"
  b=-2*v/acosh((r+q)/(2*t))
  a=b*asinh((q-r)/(2*t*sqrt(u-1)))
  d=2*sqrt((u-1)*t^5/(r+q+2*t))/(r+q-2*t)
  c=(xb+x1b+t*(q-r)/(r+q-2*t))/2
  par=neldermead([a,b,c,d],fitjohnsonu,list(X),tol=1e-6)
end
a=par(1)
b=par(2)
c=par(3)
d=par(4)
endfunction



