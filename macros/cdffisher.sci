//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdffisher(X,m,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if (m<=0)|(m~=floor(m))
  error("argument ''m'' must be an integer >= 1")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==3
  nc=0
end
if nc<0
  error("argument ''nc'' must be >= 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Xi=X(i)
  if nc==0
    Y(i)=cdff("PQ",Xi,m*ones(Xi),n*ones(Xi))
  else
    Y(i)=cdffnc("PQ",Xi,m*ones(Xi),n*ones(Xi),nc*ones(Xi))
  end
end
endfunction