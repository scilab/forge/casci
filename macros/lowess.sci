//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=lowess(X,X0,Y0,h)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=4
  error("incorrect number of arguments")
end
[n,p]=size(X)
[n0,p0]=size(X0)
if (p~=p0)
  error("arguments ''X'' and ''X0'' have incompatible sizes")
end
[ny0,py0]=size(Y0)
if (n0~=ny0)|(py0~=1)
  error("arguments ''X0'' and ''Y0'' have incompatible sizes")
end
if (h<=0)|(h>1)
  error("argument ''h'' must be in (0,1]")
end
m=ceil(h*n0)
for i=1:n
  Xi=X(i,:)
  [D,j]=sort(-sqrt(sum((ones(n0,1)*Xi-X0).^2,"c")))
  Dmax=-D(m)
  D=-D(1:m-1)
  k=j(1:m-1)
  Xk0=X0(k,:)
  Yk0=Y0(k)
  W=diag((1-(D/Dmax).^3).^3)  
  Zk0=doxpand(Xk0,"x")
  Zk0=W*Zk0
  Yk0=W*Yk0
  a=inv(Zk0'*Zk0)*Zk0'*Yk0
  Y(i)=[1,Xi]*a
end
endfunction