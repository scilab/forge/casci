//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=rndweibull(sz,a,b,c)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
select size(sz,"*")
  case 1
    row=sz(1)
    col=1
  case 2
    row=sz(1)
    col=sz(2)
  else
    error("argument ''sz'' must contain a valid vector/matrix size")
end
if (row<=0)|(row~=floor(row))|(col<=0)|(col~=floor(col))
  error("argument ''sz'' must contain a valid vector/matrix size")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if b<=0
  error("argument ''b'' must be > 0")
end
X=c+b*(-log(grand(row,col,"def"))).^(1/a)
endfunction