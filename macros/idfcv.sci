//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfcv(Y,n,cv)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if cv<=0
  error("argument ''cv'' must be > 0")
end
X=sqrt(n)./idfstudent(1-Y,n-1,sqrt(n)/cv)
endfunction
