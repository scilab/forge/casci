//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfchi2(X,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==2
  nc=0
end
if nc<0
  error("argument ''nc'' must be >= 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Xi=X(i)
  n2=n/2
  if nc==0
    Y(i)=exp(-Xi/2-gammaln(n2)).*(Xi.^(n2-1))/2^n2
  else
    Y(i)=exp(((n2-1)/2)*log(Xi/nc)-(Xi+nc)/2).*besseli(n2-1,sqrt(nc.*Xi))/2
  end
end
endfunction