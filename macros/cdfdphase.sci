//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfdphase(X,Q,q)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
q=q(:)'
Y=zeros(X)
[V,v]=spec(Q)
dv=diag(v)
iV=inv(V)
for i=find((X>=1)&(X==floor(X)))
  vr=diag(dv.^X(i))
  Y(i)=1-sum(q*real(V*vr*iV))
end
Y(Y<0)=0
endfunction