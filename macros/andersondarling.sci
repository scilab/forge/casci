//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=andersondarling(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
X=X(:)
n=length(X)
z=cdfnormal(-sort(-X),mean(X),standev(X))
a=(-sum((1:2:(2*n-1))'.*(log(z)+log(1-z(n:-1:1))))/n-n)*(1+(0.75+2.25/n)/n)
pv=1-cdfjohnson(a,"B",3.9667391,1.4859832,0.1043856,3.7372864)
endfunction