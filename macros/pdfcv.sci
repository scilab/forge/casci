//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfcv(X,n,cv)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if cv<=0
  error("argument ''cv'' must be > 0")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Xi=X(i)
  Y(i)=sqrt(n)*pdfstudent(sqrt(n)./Xi,n-1,sqrt(n)/cv)./(Xi.^2)
end
endfunction
