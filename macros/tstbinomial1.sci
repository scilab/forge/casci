//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function pv=tstbinomial1(x,n,p0,t)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if (n<1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (x<0)|(x~=floor(x))|(x>n)
  error("argument ''x'' must be an integer in {0,...,n}")
end
if (p0<0)|(p0>1)
  error("argument ''p0'' must be in [O,1]")
end
if argin==3
  t="~"
end
if t=="<"
  pv=cdfbinomial(x,n,p0)
elseif t==">"
  pv=1-cdfbinomial(x,n,p0)
elseif t=="~"
  pv=cdfbinomial(x,n,p0)
  pv=2*min(pv,1-pv)
else
  error("argument ''t'' must be ""<"", "">"" or ""~""")
end
endfunction