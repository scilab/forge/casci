//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function v=vandercorput(n,b)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
if (n<1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (b<2)|(b~=floor(b))
  error("argument ''b'' must be an integer >= 2")
end
k=floor(log(n)/log(b))+1
bb=b
nn=(1:n)'
v=zeros(n,1)
for i=1:k
 z=floor(nn/b)
 v=v+(nn-b*z)/bb
 nn=z
 bb=b*bb
end
endfunction