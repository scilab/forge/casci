//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCYN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfcpuv(X,u,v,mut,sit,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=6
  error("incorrect number of arguments")
end
if u<0
  error("argument ''u'' must be >=0")
end
if v<0
  error("argument ''v'' must be >=0")
end
if (u==0)&(v==0)
  error("argument ''u'' and ''v'' cannot be = 0 simultaneously")
end
if sit<=0
  error("argument ''sit'' must be > 0")
end
if (n<=1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
del=1/sit
gam=abs(mut)/sit
D=del*sqrt(n)
g=gam*sqrt(n)
Y=X
k=size(Y,"*")
if u==0
  for i=1:k
    if X(i)<=0
      Y(i)=0
    else
      [t,tt]=quadlegendre(127,0,D/(3*X(i)*sqrt(v)));
      z=cdfchi2((D/(3*X(i)))^2-v*t^2,n-1).*(pdfnormal(-t-g)+pdfnormal(t-g))
      if z(127)==0
        Y(i)=0
      else
        Y(i)=1-sum(tt.*z)
      end
    end
  end
elseif v==0
  for i=1:k
    if X(i)<0
      [t,tt]=quadlaguerre(127,D/u)
      z=cdfchi2(((D-u*t)/(3*X(i)))^2,n-1).*(pdfnormal(-t-g)+pdfnormal(t-g))
      Y(i)=sum(tt.*z)
    elseif X(i)>0
      [t,tt]=quadlegendre(127,0,D/u)
      z=cdfchi2(((D-u*t)/(3*X(i)))^2,n-1).*(pdfnormal(-t-g)+pdfnormal(t-g))
      Y(i)=1-sum(tt.*z)
    else
      Y(i)=1-cdfnormal(D/u-g)-cdfnormal(-D/u-g)
    end
  end
else
  for i=1:k
    if X(i)<=-u/(3*sqrt(v))
      Y(i)=0
    elseif X(i)>0
      [t,tt]=quadlegendre(127,0,D/(u+3*X(i)*sqrt(v)))
      z=cdfchi2(((D-u*t)/(3*X(i)))^2-v*t^2,n-1).*(pdfnormal(-t-g)+pdfnormal(t-g))
      Y(i)=1-sum(tt.*z)
    elseif X(i)==0
      Y(i)=1-cdfnormal(D/u-g)-cdfnormal(-D/u-g)
    else
      [t,tt]=quadlaguerre(127,D/(u+3*X(i)*sqrt(v)))
      z=cdfchi2(((D-u*t)/(3*X(i)))^2-v*t^2,n-1).*(pdfnormal(-t-g)+pdfnormal(t-g))
      Y(i)=sum(tt.*z)
    end
  end
end
Y(find(Y<0))=0
endfunction