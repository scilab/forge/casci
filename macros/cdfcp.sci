//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfcp(X,sit,n)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
if sit<=0
  error("argument ''sit'' must be > 0")
end
if (n<=1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
Y=zeros(X)
i=(X>0)
if or(i)
  Y(i)=1-cdfgamma((1)./X(i).^2,(n-1)/2,2/((n-1)*(1/(3*sit))^2))
end
endfunction