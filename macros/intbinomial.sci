//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [inter,p]=intbinomial(x,n,side,level)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
if (n<1)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if (x<0)|(x~=floor(x))|(x>n)
  error("argument ''x'' must be an integer in {0,...,n}")
end
if ~exists("side","local")
  side="both"
end
if ~exists("level","local")
  level=0.95
end
if (level<=0.5)|(level>=1)
  error("argument ''level'' must be in (0.5,1)")
end
alpha=1-level
p=x/n
select side
  case "lower"
    if x==0
      inter=0
    else
      v1=2*x
      v2=2*(n-x+1)
      inter=v1*idffisher(alpha,v1,v2)/(v2+v1*idffisher(alpha,v1,v2))
    end
  case "upper"
    if x==n
      inter=1
    else
      v1=2*(x+1)
      v2=2*(n-x)
      inter=v1*idffisher(1-alpha,v1,v2)/(v2+v1*idffisher(1-alpha,v1,v2))
    end
  case "both"
    if x==0
      inter(1)=0
    else
      v1=2*x
      v2=2*(n-x+1)
      inter(1)=v1*idffisher(alpha/2,v1,v2)/(v2+v1*idffisher(alpha/2,v1,v2))
    end
    if x==n
      inter(2)=1
    else
      v1=2*(x+1)
      v2=2*(n-x)
      inter(2)=v1*idffisher(1-alpha/2,v1,v2)/(v2+v1*idffisher(1-alpha/2,v1,v2))
    end
  else
    error("argument ''side'' must be ''lower'', ''upper'' or ''both''")
end
endfunction