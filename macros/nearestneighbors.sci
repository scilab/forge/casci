//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function i=nearestneighbors(k,x,Y,dis)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
[rx,cx]=size(x)
if rx~=1
  error("argument ''x'' must be a row vector")
end
[ry,cy]=size(Y)
if cx~=cy
  error("argument ''x'' and argument ''Y'' have incompatible sizes")
end
if (k<=0)|(k~=floor(k))|(k>ry)
  error("argument ''k'' must be an integer >= 1")
end
if argin==3
  dis="L2"
end
select dis
  case "L1"
    [u,j]=sort(-sum(abs(Y-ones(ry,1)*x),"c"))
  case "L2"
    [u,j]=sort(-sum((Y-ones(ry,1)*x).^2,"c"))
  case "Linf"
    [u,j]=sort(-max(abs(Y-ones(ry,1)*x),"c"))
  else
    error("argument ''dis'' must be ""L1"", ""L2"" or ""Linf""")
end
i=j(1:k)
endfunction