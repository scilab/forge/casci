//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfrnge(X,n,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<2)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 2")
end
if argin==2
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
X=X/sigma
Y=zeros(X)
[z,w]=quadhermite(511)
for i=find(X>0)
  Y(i)=sum(w.*pdfnormal(z).*pdfnormal(z+X(i)).*(cdfnormal(z+X(i))-cdfnormal(z)).^(n-2))
end
Y=Y*n*(n-1)/sigma
endfunction