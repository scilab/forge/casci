//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [pvsk,pvku]=mardia(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
[n,p]=size(X)
Y=X-ones(n,1)*mean(X,"r")
Z=Y*inv(varcovar(X))
ku=(mean(sum((Z).*Y,"c").^2)-p*(p+2))/sqrt(8*p*(p+2)/n)
Y=Y'
sk=0
for i=1:n
  sk=sk+sum((Z(i,:)*Y).^3)
end
pvsk=1-cdfchi2(sk/(6*n),p*(p+1)*(p+2)/6)
pvku=2*cdfnormal(-abs(ku))
endfunction