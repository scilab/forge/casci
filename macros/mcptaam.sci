//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Cp=mcptaam(X,L,U)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
[n,p]=size(X)
L=L(:)
pL=size(L,"r")
if p~=pL
  error("argument ''X'' and argument ''L'' have incompatible sizes")
end
U=U(:)
pU=size(U,"r")
if p~=pU
  error("argument ''X'' and argument ''U'' have incompatible sizes")
end
mu=mean(X,"r")
S=varcovar(X)
mu0=((U+L)/2)'
Cp=gamma(1+p/2)*prod(U-L)/((sqrt(det(S))*(%pi*idfchi2(0.9973,p))^(p/2)))
endfunction