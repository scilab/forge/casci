//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=savitzkygolay(X,p,nL,nR)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>4)
  error("incorrect number of arguments")
end
if (p<=0)|(p~=floor(p))
  error("argument ''p'' must be an integer >= 1")
end
if (nL<0)|(nL~=floor(nL))
  error("argument ''nL'' must be an integer >= 0")
end
if argin==3
  nR=nL
end
if (nR<0)|(nR~=floor(nR))
  error("argument ''nR'' must be an integer >= 0")
end
n1=nL+nR+1
if n1<p
  error("condition nL+nR+1 >= p not satisfied")
end
[row,col]=size(X)
n=row*col
X=[X(1)*ones(nL,1);matrix(X,n,1);X(n)*ones(nR,1)]
n=n+nL+nR
C=pinv(cumprod([ones(n1,1),(-nL:nR)'*ones(1,p)],"c"))
c=C(1,:)
Y=zeros(X)
for i=1:n1
  Y(nL+1:n-nR)=Y(nL+1:n-nR)+X(i:n-n1+i)*c(i)
end
Y=matrix(Y(nL+1:n-nR),row,col)
endfunction