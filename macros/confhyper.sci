//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=confhyper(X,a,b)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=3
  error("incorrect number of arguments")
end
Y=ones(X)
n=1
delta=(a/b)*X
while(or(abs(delta)>1e-8))
  Y=Y+delta
  a=a+1
  b=b+1
  n=n+1
  delta=(a/(b*n))*delta.*X
end
endfunction