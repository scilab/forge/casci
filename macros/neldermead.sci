//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [xopt,fopt]=neldermead(x0,fun,extra,tol,opt)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>5)
  error("incorrect number of arguments")
end
[j,p]=size(x0)
if j~=1
  error("argument ''x0'' must be a row vector")
end
if ~exists("extra","local")
  extra=list()
end
if ~exists("tol","local")
  tol=1e-12
end
if ~exists("opt","local")
  opt="max"
end
if typeof(fun)~="function"
  error("argument ''fun'' must be a function")
end
if typeof(extra)~="list"
  error("argument ''extra'' must be a list")
end
if tol<=0
  error("argument ''tol'' must be > 0")
end
r=1
while %t
  X=simplex(x0,r)
  for i=1:p+1
    f(i)=fun(X(i,:),extra(:))
  end
  if and(isinf(f))
    r=r/2
  else
    break
  end
end
onesp1=ones(p+1,1)
if opt=="min"
  while %t
    [f,j]=sort(f)
    X=X(j,:)
    fopt=f(p+1)
    xopt=X(p+1,:)
    fw=f(1)
    xw=X(1,:)
    xm=mean(X(2:p+1,:),"r")
    if sqrt(sum((xw-xm).^2))<=tol
      break
    end
    d=xm-xw
    xr=xw+2*d
    fr=fun(xr,extra(:))
    if fr<fopt
      xe=xr+d
      fe=fun(xe,extra(:))
      if fe<fr
        f(1)=fe
        X(1,:)=xe
      else
        f(1)=fr
        X(1,:)=xr
      end
    else
      xc=xw+0.5*d
      fc=fun(xc,extra(:))
      if fc<fopt
        f(1)=fc
        X(1,:)=xc
      else
        X=0.5*(X+onesp1*xopt)
        for i=1:p
          f(i)=fun(X(i,:),extra(:))
        end
      end
    end
  end
elseif opt=="max"
  while %t
    [f,j]=sort(f)
    X=X(j,:)
    fopt=f(1)
    xopt=X(1,:)
    fw=f(p+1)
    xw=X(p+1,:)
    xm=mean(X(1:p,:),"r")
    if sqrt(sum((xw-xm).^2))<=tol
      break
    end
    d=xm-xw
    xr=xw+2*d
    fr=fun(xr,extra(:))
    if fr>fopt
      xe=xr+d
      fe=fun(xe,extra(:))
      if fe>fr
        f(p+1)=fe
        X(p+1,:)=xe
      else
        f(p+1)=fr
        X(p+1,:)=xr
      end
    else
      xc=xw+0.5*d
      fc=fun(xc,extra(:))
      if fc>fopt
        f(p+1)=fc
        X(p+1,:)=xc
      else
        X=0.5*(X+onesp1*xopt)
        for i=2:p+1
          f(i)=fun(X(i,:),extra(:))
        end
      end
    end
  end
else
  error("argument ''opt'' must be ""min"" or ""max""")
end
endfunction