//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=pdfstudent(X,n,nc)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if (n<=0)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 1")
end
if argin==2
  nc=0
end
if nc==0
  Y=exp(gammaln((n+1)/2)-gammaln(n/2)-log(%pi*n)/2-((n+1)/2)*log(1+X.^2/n))
else
  X(find(X==0))=%eps
  Xs=sign(X)
  X2n=n+X.^2
  X2lsn=log(sqrt(2)*abs(X)./sqrt(X2n))
  C=(n*log(n)-nc^2-log(%pi)-(n+1)*log(X2n))/2-gammaln(n/2)
  i=0
  while %t
    Y=(Xs.^i).*exp(C+gammaln((n+i+1)/2)+i*log(nc)-gammaln(i+1)+i*X2lsn)
    i=i+1
    if (and(Y~=0))
      break
    end
  end
  while %t
    delta=(Xs.^i).*exp(C+gammaln((n+i+1)/2)+i*log(nc)-gammaln(i+1)+i*X2lsn)
    Y=Y+delta
    if (or(abs(delta./Y)>1e-8))
      i=i+1
    else
      break
    end
  end
end
endfunction
