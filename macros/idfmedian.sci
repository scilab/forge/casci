//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=idfmedian(Y,n,mu,sigma)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>4)
  error("incorrect number of arguments")
end
if or((Y<=0)|(Y>=1))
  error("all elements of argument ''Y'' must be in (0,1)")
end
if (n<1)|(modulo(n,2)~=1)
  error("argument ''n'' must be an odd integer >= 1")
end
if ~exists("mu","local")
  mu=0
end
if ~exists("sigma","local")
  sigma=1
end
if sigma<=0
  error("argument ''sigma'' must be > 0")
end
p=(n+1)/2
X=mu+sigma*idfnormal(idfbeta(Y,p,p))
endfunction