//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function qplot(X,dis)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if argin==1
  dis="normal"
end
if dis=="multinormal"
  [n,p]=size(X)
  if p<2
    error("argument ''X'' must have at least 2 columns")
  end
  y=X-ones(n,1)*mean(X,"r")
  u=-sort(-sum((y*inv(varcovar(X))).*y,"c"))
  z=[ones(n,1),u]
  v=idfchi2((1:n)'/(n+1),p)
else
  n=size(X,"*")
  X=matrix(X,n,1)
  u=-sort(-X)
  select dis
    case "exponential"
      v=-log(1-(1:n)'/(n+1))
    case "lognormal"
      u=u(2:n)-u(1)
      n=n-1
      u=log(u)
      v=idfnormal((1:n)'/(n+1))
    case "normal"
      v=idfnormal((1:n)'/(n+1))
    case "weibull"
      u=u(2:n)-u(1)
      n=n-1
      u=log(u)
      v=log(-log(1-(1:n)'/(n+1)))
    else
      error("argument ''dis'' must be  ""exponential"", ""lognormal"", ..
            ""multinormal"", ""normal"" or ""weibull""")
  end
end
z=[ones(n,1),u]
b=inv(z'*z)*z'*v
plot2d(u,v,-5)
plot2d(u,b(1)+b(2)*u,2)
endfunction