//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function f=fitlognormalo(par,X)
//-----------------------------------------------------------------------------
a=par(1)
b=par(2)
c=par(3)
Y=X-c
if (b<=0)|or(Y<=0)
  f=-%inf
else
  f=sum(log(b)-log(Y)-(a+b*log(Y)).^2/2)
end
endfunction
//-----------------------------------------------------------------------------
function [a,b,c]=fitlognormal(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
if (argout<3)|(argout>4)
  error("incorrect number of output arguments")
end
mu=mean(X)
sd=standev(X)
sk=skewness(X)
w=(sqrt(sk^2/4+1)+sk/2)^(1/3)-(sqrt(sk^2/4+1)-sk/2)^(1/3)
b=1/sqrt(log(w^2+1))
a=-b*log(sd^2/(w^2*(w^2+1)))/2
c=min(min(X)-0.1,mu-sd/w)
par=neldermead([a,b,c],fitlognormalo,list(X),tol=1e-8)
a=par(1)
b=par(2)
c=par(3)
endfunction