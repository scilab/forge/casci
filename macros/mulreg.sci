//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function res=mulreg(X,y)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
[n,p]=size(X)
[ny,py]=size(y)
if (n~=ny)
  error("argument ''X'' and argument ''y'' have incompatible sizes")
end
if (py~=1)
  error("argument ''y'' must be a column vector")
end
//if rank(X)<p
//  error("rank of argument ''X'' is too small")
//end
alpha=0.05
res=list()
C=inv(X'*X)
a=C*X'*y
res(2)=a
H=X*C*X'
h=diag(H)
ye=X*a
res(3)=ye
e=y-ye
res(4)=e
yb=mean(y)
SSR=sum((ye-yb).^2)
res(5)=SSR
SSE=sum((y-ye).^2)
res(6)=SSE
SST=SSE+SSR
R2=SSR/SST
res(7)=R2
PRESS=sum((e./(1-h)).^2)
R2pred=1-PRESS/SST
res(8)=R2pred
MSR=SSR/(p-1)
res(9)=MSR
res(10)=[]
res(11)=[]
res(12)=[]
res(13)=[]
res(14)=[]
res(15)=[]
res(16)=[]
if n>p
  MSE=SSE/(n-p)
  res(10)=MSE
  stda=sqrt(MSE*diag(C))
  R2a=1-(n-1)*(1-R2)/(n-p)
  res(11)=R2a
  r=e./sqrt(MSE*(1-h))
  res(12)=r
  D=(r.^2).*h./(p*(1-h))
  res(13)=D
  it=idfstudent(1-alpha/2,n-p)
  aL=a-it*stda
  res(14)=aL
  aU=a+it*stda
  res(15)=aU
  apv=2*(1-cdfstudent(abs(a)./stda,n-p))
  res(16)=apv
  sor=1-cdffisher(MSR/MSE,p-1,n-p)
end
res(17)=[]
res(18)=[]
res(19)=[]
res(20)=[]
res(21)=[]
m=0
SSPE=0
g=(y==y)
for i=1:n
  if g(i)
    m=m+1
    j=find(~or(X-ones(n,1)*X(i,:),"c"))
    SSPE=SSPE+sum((y(j)-mean(y(j)))^2)
    g(j)=%F
  end
end
res(1)=[m,n,p]
if ((n>m)&(m>p))
  res(17)=SSPE
  SSLOF=SSE-SSPE
  res(18)=SSLOF
  MSPE=SSPE/(n-m)
  res(19)=MSPE
  MSLOF=SSLOF/(m-p)
  res(20)=MSLOF
  SLOF=1-cdffisher(MSLOF/MSPE,m-p,n-m)
  res(21)=SLOF
end
endfunction