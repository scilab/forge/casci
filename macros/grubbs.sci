//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function [pv,i]=grubbs(X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=1
  error("incorrect number of arguments")
end
n=size(X,"*")
[g,i]=max(abs(X-mean(X))/standev(X))
pv=n*(1-cdfstudent(sqrt(n*(n-2)*g^2/((n-1)^2-n*g^2)),n-2))
endfunction