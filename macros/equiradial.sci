//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Z=equiradial(n,n0)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
if (n<3)|(n~=floor(n))
  error("argument ''n'' must be an integer >= 3")
end
delta=1
theta=%pi/2
u=(theta+2*%pi*(0:n-1)/n)'
Z=delta*[cos(u),sin(u)]
if argin==1
  n0=0
end
if (n0<0)|(n0~=floor(n0))
  error("argument ''n0'' must be an integer >= 0")
end
Z(abs(Z)<=%eps)=0
Z=[Z;zeros(n0,2)]
endfunction