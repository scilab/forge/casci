//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfbeta2(X,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<3)|(argin>5)
  error("incorrect number of arguments")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if b<=0
  error("argument ''b'' must be > 0")
end
if ~exists("c","local")
  c=0
end
if ~exists("d","local")
  d=1
end
if d<=0
  error("argument ''d'' must be > 0")
end
Y=zeros(X)
Z=(X-c)/d
i=(Z>0)
if or(i)
  Zi=Z(i)
  Y(i)=cdfbeta(Zi./(Zi+1),a,b)
end
endfunction