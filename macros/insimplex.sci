//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function in=insimplex(S,X)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
[n,p]=size(S)
if n~=(p+1)
  error("argument ''S'' in not a valid simplex")
end
//z=[S';ones(1,n)]
//if det(z)==0
//  error("argument ''S'' in not a valid simplex")
//end
[j,k]=size(X)
if k~=p
  error("argument ''S'' and ''X'' have incompatible sizes")
end
in=zeros(j,1)
for i=1:j
  Y=z\[X(i,:)';1]
  disp(Y)
  if and((0<=Y)&(Y<=1))
    in(i)=1
  end
end
endfunction