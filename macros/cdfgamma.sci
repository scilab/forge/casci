//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Y=cdfgamma(X,a,b,c,d)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>5)
  error("incorrect number of arguments")
end
if a<=0
  error("argument ''a'' must be > 0")
end
if ~exists("b","local")
  b=1
end
if ~exists("c","local")
  c=0
end
if ~exists("d","local")
  d=1
end
if b<=0
  error("argument ''b'' must be > 0")
end
if d==0
  error("argument ''d'' must be ~= 0")
end
Y=zeros(X)
i=(X>c)
if or(i)
  Zi=X(i)-c
  if d>0
    Y(i)=cdfgam("PQ",Zi.^d,a*ones(Zi),ones(Zi)/b^d)
  else
    Y(i)=1-cdfgam("PQ",Zi.^d,a*ones(Zi),ones(Zi)/b^d)
  end
end
endfunction