//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function X=simplex(x0,r)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<1)|(argin>2)
  error("incorrect number of arguments")
end
[j,k]=size(x0)
if j~=1
  error("argument ''x0'' must be a row vector")
end
if ~exists("r","local")
  r=1
end
if r<=0
  error("argument ''r'' must be > 0")
end
X=zeros(k+1,k)
for i=1:k
  X(1:i+1,i)=(sqrt((k+1)/(i*(i+1))))*[-ones(i,1);i]
end
X=X*r/sqrt(k)+ones(k+1,1)*x0
endfunction
