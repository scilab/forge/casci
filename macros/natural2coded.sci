//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function Z=natural2coded(Y,LU)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if argin~=2
  error("incorrect number of arguments")
end
[n,k]=size(Y)
[kk,jj]=size(LU)
if k~=kk
  error("arguments ''Y'' and ''LU'' have incompatible sizes")
end
if jj~=2
  error("argument ''LU'' must have 2 columns")
end
if or(LU(:,1)>=LU(:,2))
  error("values in the 1st column of argument ''LU'' must be < values in the 2nd column")
end
Z=zeros(Y)
for i=1:k
  Z(:,i)=(Y(:,i)-(LU(i,2)+LU(i,1))/2)/((LU(i,2)-LU(i,1))/2)
end
endfunction