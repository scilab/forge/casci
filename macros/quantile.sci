//-----------------------------------------------------------------------------
// Written by Philippe.CASTAGLIOLA@univ-nantes.fr
// Université de Nantes & IRCCyN UMR CNRS 6597
//-----------------------------------------------------------------------------
function q=quantile(X,alpha,cr)
//-----------------------------------------------------------------------------
[argout,argin]=argn()
if (argin<2)|(argin>3)
  error("incorrect number of arguments")
end
if or((alpha<=0)|(alpha>=1))
  error("argument ''alpha'' must be in (0,1)")
end
[m,n]=size(X)
alpha=1-alpha
if argin==2
  mn=m*n
  X=sort(X)
  k=alpha*(mn+1)
  if k<=1
    q=X(1)
  elseif k>=mn
    q=X(mn)
  else
    k0=floor(k)
    q=(X(k0+1)-X(k0))*(k-k0)+X(k0)
  end
elseif cr=="r"
  X=sort(X,"r")
  k=alpha*(m+1)
  if k<=1
    q=X(1,:)
  elseif k>=m
    q=X(m,:)
  else
    k0=floor(k)
    q=(X(k0+1,:)-X(k0,:))*(k-k0)+X(k0,:)
  end
elseif cr=="c"
  X=sort(X,"c")
  k=alpha*(n+1)
  if k<=1
    q=X(:,1)
  elseif k>=n
    q=X(:,n)
  else
    k0=floor(k)
    q=(X(:,k0+1)-X(:,k0))*(k-k0)+X(:,k0)
  end
else
  error("argument ''cr'' must be ""c"" or ""r""")
end
endfunction